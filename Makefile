SHELL := /bin/bash

.PHONY: help
help: ## This help dialog
	@IFS=$$'\n' ; \
	help_lines=(`fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##/:/'`); \
	printf "%-20s %s\n" "Target" "Description" ; \
	printf "%-20s %s\n" "------" "-----------" ; \
	for help_line in $${help_lines[@]}; do \
		IFS=$$':' ; \
		help_split=($$help_line) ; \
		help_command=`echo $${help_split[0]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		help_info=`echo $${help_split[2]} | sed -e 's/^ *//' -e 's/ *$$//'` ; \
		printf '\033[36m'; \
		printf "%-20s %s" $$help_command ; \
		printf '\033[0m'; \
		printf "%s\n" $$help_info; \
	done

.PHONY: ansible-deps
ansible-deps: ## Install Ansible dependencies
	ansible-galaxy collection install -r requirements.yaml

.PHONY: deploy-all
deploy-all: ## Deploy everything to the home lab
	ansible-playbook site.yaml --ask-become

.PHONY: deploy-docker
deploy-docker: ## Deploy the Docker Compose configuration
	ansible-playbook site.yaml --ask-become --tags docker

.PHONY: deploy-dns
deploy-dns: ## Deploy DNS
	ansible-playbook site.yaml --ask-become --tags dns

.PHONY: .ensure-linux
.ensure-linux: ## Ensure the platform is Linux
	"${OS}" == "Windows_NT" || echo "Run this on Linux" ; false
	

.PHONY: .safe_linux
.safe_ansible: .ensure-linux ## Copy & run Ansible from a safe temporary location
	echo "Safe"