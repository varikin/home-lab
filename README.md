# Manage my home lab

# What
Currently manages the following services on a single server.


* DNS using CoreDNS
* Docker Compose which runs
** Theater with Plex & LibArr
** Home Assistant

## How
Common commands to deploy via Ansible are defined in the Makefile.
Run `make help` for a list of targers.

## Secrets

Secrets are encrypted using [Sops](https://github.com/mozilla/sops) with a GCP KMS key.
First, make sure SOPS and [GCloud CLI](https://cloud.google.com/sdk/gcloud#download_and_install_the) is installed. The GCP example in the SOPS readme is good.

```shell
sops --encrypt --output file.enc file.conf.enc
```

Sops does magic on file types. If the input type is JSON or YAML, output should
be `file.enc.json` or `file.enc.yaml`. Otherwise, ouput file should be `file.enc`.
Otherwise, the decrypted file is a JSON or YAML file with the contents in `data` object.
Not good.

The encrypted file can be used as a lookup. See the `Copy wireguard config` task
as an example.

To decrypt a file

```shell
sops --decrypt file.enc
```
