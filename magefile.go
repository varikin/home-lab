//go:build mage

package main

import (
	"fmt"
	"io"
	"os"
	"path/filepath"

	"github.com/magefile/mage/sh"
)

var ansible = sh.RunCmd("ansible-playbook", "site.yaml")
var ansibleWithTag = sh.RunCmd("ansible-playbook", "site.yaml", "--tags")

// Installs the Ansible dependencies
func AnsibleDeps() error {
	return sh.Run("ansible-galaxy", "collection", "install", "-r", "requirements")
}

// Deploys everything to the home lab
func DeployAll() error {
	return runInTempDirectory(func() error { return ansible() })
}

// Deploys the Docker Compose configuration
func DeployDocker() error {
	return runInTempDirectory(func() error { return ansibleWithTag("docker") })
}

// Deploys DNS
func DeployDns() error {
	return runInTempDirectory(func() error { return ansibleWithTag("dns") })
}

func runInTempDirectory(target func() error) error {
	tmpDir, err := os.MkdirTemp("", "home-lab")
	if err != nil {
		return nil
	}
	defer os.RemoveAll(tmpDir)

	cwd, err := os.Getwd()
	if err != nil {
		return err
	}

	if err := copyDirectory(cwd, tmpDir); err != nil {
		return err
	}

	return target()

	return nil
}

func copyDirectory(src, dest string) error {
	entries, err := os.ReadDir(src)
	if err != nil {
		return err
	}

	for _, entry := range entries {
		srcPath := filepath.Join(src, entry.Name())
		destPath := filepath.Join(dest, entry.Name())

		fileInfo, err := os.Stat(srcPath)
		if err != nil {
			return err
		}

		switch fileInfo.Mode() & os.ModeType {
		case os.ModeDir:
			if err := createIfNotExists(destPath, 0755); err != nil {
				return err
			}
			if err := copyDirectory(srcPath, destPath); err != nil {
				return err
			}
		case os.ModeSymlink:
			return fmt.Errorf("source file is a symlink; not supported: '%s'", srcPath)
		default:
			if err := copyFile(srcPath, destPath); err != nil {
				return err
			}
		}

		fInfo, err := entry.Info()
		if err != nil {
			return err
		}
		if err := os.Chmod(destPath, fInfo.Mode()); err != nil {
			return err
		}
	}
	return nil
}

func exists(path string) bool {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}

func createIfNotExists(dir string, perm os.FileMode) error {
	if exists(dir) {
		return nil
	}

	if err := os.MkdirAll(dir, perm); err != nil {
		return fmt.Errorf("failed to create directory: '%s', error: '%s'", dir, err.Error())
	}
	return nil
}

func copyFile(srcPath, destPath string) error {
	out, err := os.Create(destPath)
	if err != nil {
		return err
	}
	defer out.Close()

	in, err := os.Open(srcPath)
	if err != nil {
		return err
	}
	defer in.Close()
	_, err = io.Copy(out, in)
	if err != nil {
		return err
	}
	return nil
}
